/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.andtinder.demo.slice;

import com.andtinder.LogUtil;
import com.andtinder.demo.ResourceTable;
import com.andtinder.model.CardModel;
import com.andtinder.model.Orientation;
import com.andtinder.view.CardContainer;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

public class MainAbilitySlice extends AbilitySlice implements CardContainer.onFlingListener {
    private String tag = "Swipeable Cards";
    private CardContainer container;
    private SimpleCardStackAdapter mProvider;
    private String description = "Description goes here";
    private String title1 = "Title1";
    private String title2 = "Title2";
    private String title3 = "Title3";
    private String title4 = "Title4";
    private String title5 = "Title5";
    private String title6 = "Title6";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mProvider = new SimpleCardStackAdapter(this);
        initData();
        initView();
    }

    private void initView() {
        container = (CardContainer) findComponentById(ResourceTable.Id_swipecard);
        container.setFlingListener(this);
        container.setOrientation(Orientation.Disordered);
        container.setItemProvider(mProvider);
        mProvider.setContainer(container);
    }

    private void initData() {
        mProvider.add(new CardModel(title1, description, ResourceTable.Media_picture1));
        mProvider.add(new CardModel(title2, description, ResourceTable.Media_picture2));
        mProvider.add(new CardModel(title3, description, ResourceTable.Media_picture3));
        mProvider.add(new CardModel(title4, description, ResourceTable.Media_picture1));
        mProvider.add(new CardModel(title5, description, ResourceTable.Media_picture2));
        mProvider.add(new CardModel(title6, description, ResourceTable.Media_picture3));
        mProvider.add(new CardModel(title1, description, ResourceTable.Media_picture1));
        mProvider.add(new CardModel(title2, description, ResourceTable.Media_picture2));
        mProvider.add(new CardModel(title3, description, ResourceTable.Media_picture3));
        mProvider.add(new CardModel(title4, description, ResourceTable.Media_picture1));
        mProvider.add(new CardModel(title5, description, ResourceTable.Media_picture2));
        mProvider.add(new CardModel(title6, description, ResourceTable.Media_picture3));
        mProvider.add(new CardModel(title1, description, ResourceTable.Media_picture1));
        mProvider.add(new CardModel(title2, description, ResourceTable.Media_picture2));
        mProvider.add(new CardModel(title3, description, ResourceTable.Media_picture3));
        mProvider.add(new CardModel(title4, description, ResourceTable.Media_picture1));
        mProvider.add(new CardModel(title5, description, ResourceTable.Media_picture2));
        mProvider.add(new CardModel(title6, description, ResourceTable.Media_picture3));
        mProvider.add(new CardModel(title1, description, ResourceTable.Media_picture1));
        mProvider.add(new CardModel(title2, description, ResourceTable.Media_picture2));
        mProvider.add(new CardModel(title3, description, ResourceTable.Media_picture3));
        mProvider.add(new CardModel(title4, description, ResourceTable.Media_picture1));
        mProvider.add(new CardModel(title5, description, ResourceTable.Media_picture2));
        mProvider.add(new CardModel(title6, description, ResourceTable.Media_picture3));
        mProvider.add(new CardModel(title1, description, ResourceTable.Media_picture1));
        mProvider.add(new CardModel(title2, description, ResourceTable.Media_picture2));
        mProvider.add(new CardModel(title3, description, ResourceTable.Media_picture3));
        mProvider.add(new CardModel(title4, description, ResourceTable.Media_picture1));
        mProvider.add(new CardModel(title5, description, ResourceTable.Media_picture2));
    }

    @Override
    public void removeFirstObjectInAdapter() {
        // this is the simplest way to delete an object from the Adapter (/AdapterView)
        LogUtil.debug("LIST", "removed object!");
        mProvider.getData().remove(0);
        mProvider.notifyDataSetItemRemoved(0);
    }

    @Override
    public void onLeftCardExit(Object dataObject) {
        LogUtil.debug(tag, "I dislike the card");
    }

    @Override
    public void onRightCardExit(Object dataObject) {
        LogUtil.debug(tag, "I like the card");
    }

    @Override
    public void onCardClick(Object dataObject) {
        LogUtil.debug(tag, "I press the card");
    }
}
