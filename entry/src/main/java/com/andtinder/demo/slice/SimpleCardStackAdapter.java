/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andtinder.demo.slice;

import com.andtinder.demo.ResourceTable;
import com.andtinder.model.CardModel;
import com.andtinder.view.CardContainer;
import ohos.agp.components.*;
import ohos.agp.text.Font;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;

/**
 * provider
 */
public class SimpleCardStackAdapter extends BaseItemProvider {
    private Context mContext;
    private final Object lock = new Object();
    private ArrayList<CardModel> data = new ArrayList<>();
    private CardContainer cardContainer;

    /**
     * constructor
     *
     * @param context context
     */
    public SimpleCardStackAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public CardModel getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer container) {
        MyComponentHolder holder;
        if (convertView == null) {
            convertView = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item, container, false);
            holder = new MyComponentHolder();
            convertView.setTag(holder);
        } else {
            holder = (MyComponentHolder) convertView.getTag();
        }
        holder.title = (Text) convertView.findComponentById(ResourceTable.Id_title);
        holder.title.setText(data.get(position).getTitle());
        holder.title.setFont(Font.DEFAULT_BOLD);

        holder.image = (Image) convertView.findComponentById(ResourceTable.Id_image);
        holder.image.setPixelMap(data.get(position).getCardImageDrawable());
        holder.image1 = (Image) convertView.findComponentById(ResourceTable.Id_image_1);
        holder.image1.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                cardContainer.setNotNeedRotation();
                return false;
            }
        });
        holder.image2 = (Image) convertView.findComponentById(ResourceTable.Id_image_2);
        holder.image2.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                cardContainer.setNotNeedRotation();
                return false;
            }
        });
        holder.description = (Text) convertView.findComponentById(ResourceTable.Id_description);
        holder.description.setText(data.get(position).getDescription());
        return convertView;
    }

    /**
     * item component holder
     */
    public static class MyComponentHolder {
        private Text title;
        private Image image;
        private Image image1;
        private Image image2;
        private Text description;
    }

    /**
     * add cardmodel
     *
     * @param item
     */
    public void add(CardModel item) {
        synchronized (lock) {
            data.add(item);
        }
        notifyDataChanged();
    }

    public ArrayList<CardModel> getData() {
        return data;
    }

    public void setData(ArrayList<CardModel> data) {
        this.data = data;
    }

    public CardContainer getContainer() {
        return cardContainer;
    }

    public void setContainer(CardContainer container) {
        this.cardContainer = container;
    }
}
