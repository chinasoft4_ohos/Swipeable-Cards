package com.andtinder.demo;

import com.andtinder.model.CardModel;
import com.andtinder.model.Orientation;
import com.andtinder.view.CardContainer;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.andtinder.demo", actualBundleName);
    }

    @Test
    public void testsetOrientation() {
        CardContainer cardContainer = new CardContainer(MyApplication.getApplication());
        cardContainer.setOrientation(Orientation.Disordered);
        assertEquals(Orientation.Disordered, cardContainer.getOrientation());
    }

    @Test
    public void testsetDescription() {
        CardModel cardModel = new CardModel();
        cardModel.setDescription("description");
        assertEquals("description", cardModel.getDescription());
    }

    @Test
    public void testsetTitle() {
        CardModel cardModel = new CardModel();
        cardModel.setTitle("title");
        assertEquals("title", cardModel.getTitle());
    }
}