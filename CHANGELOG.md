## 1.0.0
* 正式版发布

## 0.0.1-SNAPSHOT
ohos 第一个版本
 * 实现了原库的大部分 api
 * 基于ComponentContainer对原库进行了重写
 * 原onLike改为onLeftCardExit，原onDislike改为onRightCardExit，原CardModel.OnClickListener()改为onCardClick
