/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andtinder.model;

public enum Orientation {
    /**
     * 组件初始化时是否有序
     */
    Ordered, Disordered;

    /**
     * get value from index
     *
     * @param index
     * @return return value
     * @throws IndexOutOfBoundsException IndexOutOfBoundsException
     */
    public static Orientation fromIndex(int index) {
        Orientation[] values = Orientation.values();
        if (index < 0 || index >= values.length) {
            throw new IndexOutOfBoundsException();
        }
        return values[index];
    }
}

