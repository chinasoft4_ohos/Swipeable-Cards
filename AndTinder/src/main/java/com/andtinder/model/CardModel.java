/**
 * AndTinder v0.4 for Android
 *
 * @Author: Enrique López Mañas <eenriquelopez@gmail.com>
 * http://www.lopez-manas.com
 * <p>
 * TAndTinder is a native library for Android that provide a
 * Tinder card like effect. A card can be constructed using an
 * image and displayed with animation effects, dismiss-to-like
 * and dismiss-to-unlike, and use different sorting mechanisms.
 * <p>
 * AndTinder is compatible with API Level 19 and upwards
 *
 * @copyright: Enrique López Mañas
 * @license: Apache License 2.0
 */

package com.andtinder.model;

public class CardModel {
    private String title;
    private String description;
    private int cardImageDrawable;

    /**
     * constructor
     */
    public CardModel() {
        this(null, null, 0);
    }

    /**
     * constructor
     *
     * @param title
     * @param description
     * @param cardImage
     */
    public CardModel(String title, String description, int cardImage) {
        this.title = title;
        this.description = description;
        this.cardImageDrawable = cardImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCardImageDrawable() {
        return cardImageDrawable;
    }

    public void setCardImageDrawable(int cardImageDrawable) {
        this.cardImageDrawable = cardImageDrawable;
    }
}
